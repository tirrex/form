//обрабочик нажатия на переключатель языка - включение RU
$("a#ru").click(function() {
    //ajax запрос
     $.ajax({
   url: '/default/setlang?lang=ru',
  success: function(){
  
    window.location.reload();
  }
});
});
//обрабочик нажатия на переключатель языка - включение EN
$( "a#en" ).click(function() {
    //ajax запрос
  $.ajax({
   url: '/default/setlang?lang=en',
  success: function(){
  
    window.location.reload();
  }
});
});

//подключение валидатора полей формы
$("#signupform").validate();



//функция валидации загружаемого файла
$('input[type="submit"]').prop("disabled", true);
var a=0;
//назначение обработчика к полям загрузки файлов
$('#picture').bind('change', function() {
if ($('input:submit').attr('disabled',false)){
	$('input:submit').attr('disabled',true);
	}
var ext = $('#picture').val().split('.').pop().toLowerCase();
if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1){
	$('#error1').slideDown("slow");
	$('#error2').slideUp("slow");
	a=0;
	}else{
	var picsize = (this.files[0].size);
	if (picsize > 1000000){
	$('#error2').slideDown("slow");
	a=0;
	}else{
	a=1;
	$('#error2').slideUp("slow");
	}
	$('#error1').slideUp("slow");
	if (a==1){
		$('input:submit').attr('disabled',false);
		}
}
});

