<?php
//главный класс приложения
class App {
    //параметры приложения
     //mysql server
    //const SERVER = '46.101.200.223';
    public $server ;    //сервеh mysql
    //username and password
//    const USERNAME = 'remote';
//    const PASS = 'Sudoku!!!$$$';
    public $username ; // имя пользвателя для базы mysql
    public $pass ;    // пароль для базы mysql
    public $default_language ;  //язык по умолчанию
    public $languages ; //список поддкрживаемых языковлокализации  

    public $db ; //имя базы данных mysql

    public $path_to_controllers ; //путь к файлам контроллеров
    
    public $path_to_views;   //путь к шаблонам 
   
    public $path_to_layouts ;   //путь к шаблонам страниц  
    public $main_layout ;    //главный шаблон страниц
    public $upload_dir ;   //имя каталога загрузок
    public $image_dir; //каталого картинок
    public $default_user; //имя плоьзователя по умолчанию для записи в сессию
    
    //переменная для хранения экзепляра класса
    private static $_instance = null;
  
    //закрываем __clone
    private function __clone()
    {
    }
   //закрываем __wakeup
    private function __wakeup()
    {
    }
    
 // защищенный конструктор - для запрета создания другого экзмпляра класса
    private function __construct() {
   
        //заполняем параметры приложения по константам из классса Config
          foreach (array_keys (get_class_vars('App')) as $var ) {
              if (substr($var, 0, 1) !=='_') {
                $constname = strtoupper($var);
                $constval = 'Config::'.$constname;
                $this->$var = constant($constval);
              }
              
            }
            //формируем массив доступных языков
            $this->languages = explode('|',$this->languages);
    }
    //получение экземпляра класса приложения 
    static public function getInstance() {
        if(is_null(self::$_instance)){

            self::$_instance = new self();
        }
        return self::$_instance;
    }

    //wrapper для выборки локализованного текста в зависимости от установленного  языка
    //переменная в сесси - lang
    public function t($message) {
        return LangContext::getMessage($message);   
    }
    //запуск приложения
    public function run() {
        //соединение с базой данных MySQL
        MySqlConnector::connect();
        
        session_start();
        //установка текущего пользователя
        if (!isset($_SESSION['authuid'])) $_SESSION['authuid']=$this->default_user;
        //установка текущего языка
        if (!isset($_SESSION['lang'])) $_SESSION['lang']=$this->default_language;
        //запуск главного роутера приложения
        $this->mainRouter();
               
    }
    //главный роутер приложения, разбирает url и запускает соотв контроллер с действием в параметре
    private function mainRouter() {
               
        //разбор текущего запроса в массив RequestUrl
        $RequestUrl = parse_url($_SERVER['REQUEST_URI']) ;
        //разбивка переменной path на составляющие
        $UrlPathArray = explode('/',$RequestUrl['path']);
        
        //фильтруем переменную полученную из url
        $controllername = ucfirst(strtolower(filter_var(trim($UrlPathArray[1]), FILTER_SANITIZE_STRING))) ;
        
        //ищем текущее действие для определенного ранее контроллера - это подкаталог второго уровня
        $actionname = 'default';//действие по умолчанию
        if (isset($UrlPathArray[2])){//если действие определено
            $actionname = filter_var(trim($UrlPathArray[2]), FILTER_SANITIZE_STRING); //очистка переменной
        }
        //если не определен контроллер т.е. нет ни одного подкаталога в url относительно адреса сайта - назнаем контроллер по умолчанию
        //(если подкаталог физически существует, то этот путь отсекается на уровне htaccess и отображается вебсервером напрямую)
        if ($UrlPathArray[1]==='' ) {//если не определен подкаталог в url
            //контроллер по умолчанию
            $controllerclassname = 'DefaultController';
        }
        else { //подкаталог есть, считаем что это - имя контроллера
        
            //если существует класс контроллера - определяем его в переменной $controllerclassname
            if (file_exists($_SERVER['DOCUMENT_ROOT'].  $this->path_to_controllers.$controllername.'Controller.php')) {
                $controllerclassname = $controllername.'Controller';       
            }
            else { //класс контроллера не существует - отдаем ошибку 404
                header("HTTP/1.1 404 Not Found");
                die($this->t("URL does not found")); 
            }      
        }
        //создание объекта контроллера
        $controller = new $controllerclassname ;
        //запуск контроллера с параметром - действием     
        $controller->run($actionname);
        
    }
}
