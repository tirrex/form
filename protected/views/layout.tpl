<!DOCTYPE html>
<html lang="{LANG}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>{TITLE}</title>
      
    <!-- Bootstrap core CSS -->
    <link href="{HTTP_HOST}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{HTTP_HOST}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{HTTP_HOST}/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{HTTP_HOST}/css/signin.css" rel="stylesheet">
    <link href="{HTTP_HOST}/css/jumbotron-narrow.css" rel="stylesheet">
    <link href="{HTTP_HOST}/css/testapp.css" rel="stylesheet" type="text/css"/>

  <body>
{MAINMENU}
    

      <div class="jumbotron">
        {CONTENT}
      </div>


      <footer class="footer">
        <p  style="float: left;">&copy;Copyright</p><div style="float: right;"><a href="#" class="{RUOPT}" id="ru">RU</a>&nbsp;<a href="#" class="{ENOPT}" id="en">EN</a></div>
      </footer>

   
    <script src="{HTTP_HOST}/js/jquery-1.11.1.js" type="text/javascript"></script>
    <script src="{HTTP_HOST}/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{HTTP_HOST}/js/jquery.validate.js" type="text/javascript"></script>
    <script src="{HTTP_HOST}/js/additional-methods.js" type="text/javascript"></script>
    {VALIDATERUMESSAGES}
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{HTTP_HOST}/js/ie10-viewport-bug-workaround.js"></script>
    <script src="{HTTP_HOST}/js/ie8-responsive-file-warning.js" type="text/javascript"></script>
    <script src="{HTTP_HOST}/js/raw-files.min.js" type="text/javascript"></script>
    <script src="{HTTP_HOST}/js/ie-emulation-modes-warning.js"></script>
    <!--[if lt IE 10]>
    <script src="{HTTP_HOST}/js/ie9placeholders.js" type="text/javascript"></script>
    <![endif]-->
      <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="{HTTP_HOST}/js/ie8-responsive-file-warning.js"></script><![endif]-->
   
  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{HTTP_HOST}/js/testapp.js" type="text/javascript"></script>
  </body>
</html>
