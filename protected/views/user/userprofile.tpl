<h2>User profile</h2>
 <div class="form-group">
  <label for="login" class="col-sm-2 control-label">First name</label>
  <div class="col-sm-10">
  <p class="form-control-static">{FIRSTNAME}</p>
  </div>
 </div>
   <div class="form-group">
  <label for="login" class="col-sm-2 control-label">Last name</label>
  <div class="col-sm-10">
  <p class="form-control-static">{LASTNAME}</p>
  </div>
 </div>
<form class="form-horizontal">
 <div class="form-group">
  <label class="col-sm-2 control-label">Email</label>
  <div class="col-sm-10">
   <p class="form-control-static">{EMAIL}</p>
  </div>
 </div>
 <div class="form-group">
  <label for="login" class="col-sm-2 control-label">Login name</label>
  <div class="col-sm-10">
  <p class="form-control-static">{LOGIN}</p>
  </div>
 </div>
   <div class="form-group">
  <label for="picture" class="col-sm-2 control-label">Uploaded picture</label>
  <div class="col-sm-10">
      <p class="form-control-static"><img class="img-rounded" src="{FILENAME}" height = "200" width="250"></p>
  </div>
 </div>
</form>