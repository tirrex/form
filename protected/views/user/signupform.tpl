
<form class="form-signin" method="post" id="signupform" enctype="multipart/form-data">
   
  {ALERT}
          
    <h2 class="form-signin-heading">User registration form</h2>
    <input type="text" id="loginname" class="form-control"  name="login" placeholder="Login name" minlength="4" data-rule-required="true" autofocus >
    
    <input type="text" id="firstname" class="form-control"  name="firstname" placeholder="First name "  data-rule-lettersonly="true"  required >
    
    <input type="text" id="lastname" class="form-control"  name="lastname" placeholder="Last name "  data-rule-lettersonly="true"  required >

   
    <input type="email" id="email" class="form-control" placeholder="Email"  name="email" required >
    <input type="password" id="password1" class="form-control" placeholder="Password"  name="password" required>
    <input type="password" id="password2" class="form-control" placeholder="Password retype" data-rule-equalto="#password1" required>
    

     <div class="input-group">
        <label class="input-group-btn">
        <span class="btn btn-primary">
            Browse&hellip; <input id="picture" name ="picture" type="file" style="display: none;" data-rule-extension="" >
           
        </span>
        </label>
         
        <input type="text" class="form-control" placeholder="Pick a picture" readonly>
    </div>
    <p id="error1" style="display:none; color:#FF0000;">
        Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.
    </p>
    <p id="error2" style="display:none; color:#FF0000;">
        Maximum File Size Limit is 1MB.
    </p>
    
    <hr>
    <button class="btn btn-lg btn-primary btn-block" name="register" type="submit">Register</button>

    

    
</form>
    
