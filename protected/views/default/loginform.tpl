<form class="form-signin" method="post" id="signupform">
   
  {ALERT}
          
    <h2 class="form-signin-heading">Login form</h2>
    <label for="inputEmail" class="sr-only">Email</label>
 
    <input type="text" id="login" class="form-control"  name="login" placeholder="Login name" data-rule-alphanumeric="true" data-rule-minlength="4" data-rule-required="true" autofocus>
    <label for="inputPassword" class="sr-only" >Password</label>
    <input type="password" id="password" class="form-control" placeholder="Password " data-rule-required="true" name="password" >

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="registenewruser">Login</button>
</form>