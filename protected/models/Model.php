<?php
//базовый класс модели данных
class Model {

   //поиск и загрузка модели из базы данных
    //$params - массив параметров запроса к БД
    function getModel($params) {
        $App=App::getInstance();
        $result = false;
        $ParamsString = '';
        //вызов функции подготовки параметров для формирования запроса
        $ParamsArray = $this->prepareParams($params);    
        //если нет ошибок
        if ($ParamsArray) {
            $counter = 0;
            //фомируем строку запроса
            foreach ($ParamsArray as $field => $value) {
                $counter++;
                if ($counter === count($ParamsArray) ){
                    $ParamsString = $ParamsString." $field = $value ";
                }
                else{
                    $ParamsString = $ParamsString." $field = $value AND ";
                }               
              
            }
            //имя таблицы БД
            $tablename = $this->tableName();
            //строка запроса
            $sql = "SELECT * FROM $tablename WHERE $ParamsString LIMIT 1";
            //выполнение запроса
            $mysqlresult = @mysql_query($sql) or die($sql.' '.mysql_error());
            //если резутат успешный - заполнение переменных текущего объекта данными из базы
            if (mysql_num_rows ($mysqlresult)){

                $sqlresult = mysql_fetch_array($mysqlresult) ;
                
                if ($sqlresult){
                    foreach ($sqlresult as $key => $value) {
                        $this->$key = $value;
                    }
                    //флаг успешного резульатта выборки - пользователь найден
                    $result = true;
                }

            }
            
        }
        //если не бл получен результат - возвращаем false
        return $result;
    
    }
    //проврка и обработка данных из формы регистрации
    function validate ($field, $rule) {
        $App=App::getInstance();
        $result = '';
        switch ($rule) {
            case 'unique': // проверка на уникальность значения поля в БД
                $tempmodel = new Users;
                if ($tempmodel->getModel([$field => $this->$field])){
                    $result = $App->t(' Value: ').$this->$field.$App->t(' is already present in database, please provide another one ');
                }
                break;
            case 'required': //проверка заполнения
                if (trim($this->$field)==='') $result = $App->t(' Field ').$field.$App->t(' is required ');
                break;
            case 'string'://проверка на формат - строка
                if (ctype_alpha(trim($this->$field))===false) $result = $App->t(' Field ').$field.$App->t(' must be a string ');
                break;
            case 'email': //проверка шаблона ввода email
                if (filter_var($this->$field,FILTER_VALIDATE_EMAIL)===false) $result = $App->t('Email field value does not match the format');
                break;     
            case 'md5' : //обработка поля - шифрование с помощью 
                $this->$field = md5($this->$field);
                break;
            case 'extension' : //проверка расширения загруженного файла
             
                list($field,$allowedext) = $field;       
                $ext = pathinfo($this->$field, PATHINFO_EXTENSION);         
                if (strripos($rule, $ext) === false) $result = $ext.$App->t(' is not allowed extension, please choose file with one of theese formats: jpg, png, gif');        
                break;
            default:
                break;
        }
        return $result;
    }
    
    //функци валидации модели
    function validateModel() {
        $App=App::getInstance();
        $result = ''; //сюда будем записывать сообщения об ошибках
        //получениеи правил валидации
        $rules = $this->getRules();
        //перебор правил и вызов валидации для каждого поля и правила
        foreach ($rules as  $rule => $fields) {
            if (is_array($fields)){
                foreach ($fields as $fieldname) {
                     $result = $result.$this->validate($fieldname, $rule);                                         
                }
            }
            else {
                $result = $result.$this->validate($fields, $rule);
            }
                                          
        }
        if (trim($result) !== '') { // если есть ошибки возвращаем их
            return $result;
        } else {
            return true; //ошибок нет
        }
    }
    //функци сохранения модели
    function saveModel() {
        $App=App::getInstance();
        $result=false; //результат работы
            
        $FieldsString = ''; //строка полей
        $ValuesString = ''; //строка значений
        //формирование массива параметров запроса
        foreach ($this as $key => $value) {
            $params[$key] = $value;
        }
        //подготовка параметров для запроса
        $ParamsArray = $this->prepareParams($params); 
        //если параметры без ошибок
        if ($ParamsArray) {
            //формируем строку полей
            $FieldsString = ' ('.implode (',', array_keys($ParamsArray)).') ';
             //формируем строку значений
            $ValuesString = ' ('.implode (',', array_values($ParamsArray)).') ';
            //имя таблицы mysql
            $tablename = $this->tableName();
            //строка запроса
            $sql = "INSERT INTO $tablename $FieldsString VALUES $ValuesString ";
            //запрос
            $mysqlresult = @mysql_query($sql) or die($sql.' '.mysql_error());
            if ($mysqlresult){//если запросм выполнен успешно

                $result = mysql_insert_id() ; //возвращаем новый id записи

            }
        }
       
        return $result;
    } 
    //подготовка параметров для запроса mysql
     private function prepareParams($params) {
        $result = false;
        if (is_array($params)) {//если параметры переданы 
            foreach ($params as $field => $value) { 
                //экранируем спец символы и заключаем значения в кавычки 
                $params[$field] = "'".mysql_real_escape_string($value)."'";
            }
            $result = $params;
        }
    
        return $result;
        
    }
    

}