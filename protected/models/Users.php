<?php
//класс модели пользователя
class Users extends Model{
    //параметры пользователя
    public $uid; // id
    public $firstname; //имя
    public $lastname; //фамилия
    public $email; //email
    public $login; //логин
    public $password; //пароль
    public $filename; //имя файла картинки
    //определение правил для валидации модели
    function getRules() { 
        return [
        'required' => ['firstname','lastname','email','login','password'],//обязательные поля
        'string' => ['firstname','lastname'],//только буквы
        'email' => 'email',//формат email
        'unique' => ['login','email'],//уникальные значения
        'md5' => ['password'],  //замена значения его md5 хешем
        'filextension' => 'filename=jpg,jpeg,gif,png'   //проверка расширения загруженного файла
        ];
    }
    //имя таблицы БД
    function tableName() {
        return 'users';
    }
          
}
