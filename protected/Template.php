<?php
// класс шаблонизатора

class Template  {
    var $values     = array();	// переменные шаблона
    var $html;				// HTML-код

// функция загрузки шаблона
    function getTpl($tpl_name)
      {
      if(empty($tpl_name) || !file_exists($tpl_name))
        {
        return false;
        }
      else
        {
        $this->html  = join('',file($tpl_name));
        }
      }

// функция установки значения
    function setValue($key,$var)
      {
      $key = '{' . $key . '}';
      $this->values[$key] = $var;
      }

// парсинг и заполнение шаблона
    function tplParse(){
      //замена плейсхолдеров на значения
        foreach($this->values as $find => $replace){

            $this->html = str_replace($find, $replace, $this->html);
        }
        // очистка не заданных плейсхолдеров
        $this->html = preg_replace('/\{[\s\S]*?\}/', '', $this->html); 

    //    session_start();
        
       //текущий язык
        $currlanguage = $_SESSION['lang'];
        //если задан не английский язык - ищем перевод
        if ($currlanguage !== 'EN') {
            $this->html = str_replace(array_keys(LangContext::$messages[$currlanguage]),  array_values(LangContext::$messages[$currlanguage]),$this->html);
        }
        
        
            
    }

}    

   
