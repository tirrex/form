<?php
//класс пользователя
class UserController extends Controller{
    //имя контроллера
    function controllerName() {
        return 'user';
    }
    //правила доступа к действиям: * - все, private  - только конкретный  пользователь
    function accessRules() {
        return [
            'default' => 'private',
            'register' => '*'
        ];
    }
    //выход из системы
    function actionLogout(){
        $App=App::getInstance();
        session_start();
        //проверяем ид залогиненного пользователя
        if ($_SESSION['authuid'] === 'guest'){ // если гость
            $content =  'Nobody have been logged in!'; //сообщение что никто не логинился 
        }
        else {
            $_SESSION['authuid'] = 'guest'; // если был кто-то залогинен - установка переменной авторизаиии сообщение
            $content =  'You are logged out!';
        }
        //вывод страницы выхода из аккаунта
        $this->RenderPage($content,$App->t('Logout'));
    }
        
    //действие по умолчанию
    function actionDefault(){
        $App=App::getInstance();
        session_start();
        $result = false;
        //если не передан ид пользователя - отдаем ошибку
        if (isset($_GET['uid'])){
           
            //очистка данных из url, плучаем id пользователя
            $userid = filter_input(INPUT_GET, 'uid',FILTER_VALIDATE_INT );
            //проверка прав доступа для этого действия и страницы
            if ($this->CheckAccess('default',strval($userid))) {
                
                if ($userid !== false ){//uid из url прошел проверку
                    $user = new Users;             

                    if ($user->getModel(['uid'=>$userid])){ //ищем модель пользователя по uid из url
                        //параметры  для заполнения шаблона профиля
                        $params = [
                            'email' => $user->email,
                            'login' => $user->login,
                            'firstname' => $user->firstname,
                            'lastname' => $user->lastname,
                             'filename' => $user->filename
                        ];
                        //генерируем страницу профиля
                        $content = $this->RenderTemplate($params, 'userprofile.tpl');
                        $this->RenderPage($content,$App->t('User Profile'));
                        $result = true;
                    }         
                }
                
            } 
            else {//если пользователь не меет прав доступа - ошибка 403
                header("HTTP/1.1 403 Forbidden");
                die($App->t("Access denied"));  
            }
        }
        if(!$result) {//если не задан uid - ошибка 404
            header("HTTP/1.1 404 Not Found");
            die($App->t("Requested page does not found"));  
        }
    }
    //действие регистрации пользователя  
    function actionRegister() {
        $App=App::getInstance();
        
        $result = false;
        //проверка наличия в POST параметров формы регитрации
        if (!isset($_POST['register'])){
            //если нет - выводим форму регистрации
            $content = $this->RenderTemplate(['alert' => ''], 'signupform.tpl');
            $this->RenderPage($content,$App->t('User registration form'));
        }
        else {//нажата кнопка Регистрации
            // фильтры параметров формы 
            $definition = [  
                'login' => FILTER_SANITIZE_STRING,              
                'email' => FILTER_SANITIZE_EMAIL,
                'password' =>  FILTER_UNSAFE_RAW,
                'firstname' => FILTER_SANITIZE_STRING,
                'lastname' => FILTER_SANITIZE_STRING,
            ];
            //фильтрация
            $form_data = filter_input_array(INPUT_POST, $definition);
            //проверка результата
            if(in_array(null, $form_data, true)===false) { //если нет ошибок
                //создаем нового пользователя          
                $newuser = new Users;
                //заполняем модель данными из формы
                foreach ($newuser as $key => $value) {
                    if (isset($form_data[$key])){
                        $newuser->$key = $form_data[$key];                       
                    }                        
                }
                //если был загружен файл               
                if (file_exists($_FILES['picture']['tmp_name'])){
                    $md5 = md5($_FILES['picture']['tmp_name']); //md5 загруженного файла
                    $filename = 'Pic'.$md5.'.'.pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION); //новое имя файла 
                    $fullpathtouploadfile=$_SERVER['DOCUMENT_ROOT'].Config::UPLOAD_DIR.$filename ; //полный путь к новому файлу
                  if (move_uploaded_file($_FILES['picture']['tmp_name'], $fullpathtouploadfile)) { //если удалось перемстить в каталог загрузок
                      //записываем имя файла в поле объекта пользователя
                        $newuser->filename=$App->upload_dir.$filename;
                    }
                }                                
                else { //файл не был указан или неудача при загрузке
                    $newuser->filename=$App->image_dir.'noimage.png';//заглушка для картинки
                }
                //проверка модели нового пользователя
                $errormessage = $newuser->validateModel();
                //если нет ошибок - сохраняем модель в базе данных
                if ($errormessage === true){
                    
                    $result = strval($newuser->saveModel());                   
                }
            } 
            else { //если были ошибки при валидации формы
                $errormessage = $App->t("Form validation error, please, check input data");
            }
            if ($result === false){ // если были ошибки проверки валидации формы 
                //формируем сообщение об ошибках
                $message = $this->RenderTemplate(['message' => $errormessage], 'errormessage.tpl');
                $content = $this->RenderTemplate(['alert' => $message], 'signupform.tpl');
                //форируем страницу
                $this->RenderPage($content,$App->t('User registration error'));
            }
            else {//если присланные данные верны
                session_start();
                //запмсываем ид нового пользоваеля в сессию
                $_SESSION['authuid']=$result;
                //редирект на профиль нового пользователя
                header('Location: /user?uid='.$result);
                
            }
        }
    }                                    
}
