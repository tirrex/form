<?php
//контроллер по умолчанию - если не задан
class DefaultController extends Controller{
    //установка языка по get запросу - в переменной lang
    function actionSetlang() {
        $App=App::getInstance();
       // session_start();
        //переменная установки языка
        $langid = strtoupper(filter_input(INPUT_GET, 'lang' ,FILTER_SANITIZE_STRING ));
        
        var_dump($App->languages);
        var_dump($langid);
        var_dump($_SESSION['lang']);
        //если локаль есть в списке - устанавливаем переменную lang в сессию
        if (in_array($langid,$App->languages)){
            $_SESSION['lang']=$langid;
        }
    }
    //действие по умолчанию - если не задано
    function actionDefault() {
        $App=App::getInstance();
    //    session_start();
        $errormessage = '';
        //если нет параметров POST - выводим форму логина
        if (!isset($_POST['login']) || !isset($_POST['password'])){

            $content = $this->RenderTemplate(['alert' => ''], 'loginform.tpl');
            $this->RenderPage($content, $App->t('Login form'));
        }
        else { //получили параметры из формы
            //фильтры для данных из формы
            $definition = array(
                'login' => FILTER_VALIDATE_STRING,              
                'password' => FILTER_UNSAFE_RAW
            );
            //фильтрация
            $form_data = filter_input_array(INPUT_POST, $definition);
            //если переменная удаленна фильтром - выводим сообщение об ошибке $errormessage
            if(in_array(null, $form_data, true)) {
 
                $errormessage = $App->t('Form validation error, please, check input data');
                  
            } else { //если ошибок фильтрации нет - данные безопасны, можно продолжать
                
                $usermodel = new Users;  // новый объект пользователя            
                //поиск пользователя по данным из формы логина
                $isuser = $usermodel->getModel(['login' => $form_data['login'], 'password' => md5($form_data['password'])]);
                //если найден - редирект на его профиль и запись его ид в переменную сессии                        
                if ($isuser ){
                    $_SESSION['authuid']=$usermodel->uid;
                    header('Location: /user?uid='.$usermodel->uid);
                                    
                }
                else { // если такого пользователя нет - выводим сообщение об ошибке
                          
                    $errormessage = $App->t('Providing login and password do not match any registered user, please, check input data or Register!');
                   
                }
               
            }                     
            //выводим сообщение об ошибке и формируем сраницу формы входа
           if ($errormessage!==''){
               $params = [
                        'type' => 'alert alert-danger',
                        'header' => $App->t('Error!'),
                        'message' => $errormessage
                    ];
                //блок сообщения об ошибке
                $errorform = $this->RenderTemplate($params, 'message.tpl');
                //формируем форму авторизации
                $content = $this->RenderTemplate(['alert' => $errorform], 'loginform.tpl');
                //формируем сраницу формы авторизации
                $this->RenderPage($content,'Login form');
           }
            
        }   
                
    }
                        
}

