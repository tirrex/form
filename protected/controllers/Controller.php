<?php
//базовый класс контроллера
class Controller {
    //правила доступа к действию по умолчанию: * - все
    function accessRules() {
        return [
            'default' => ['*'],
        ];
    }
    //возват имени контроллера
    function ControllerName() {
        return 'default';
    }
    //запуск действия с параметром 
    //$action- имя действия 
    function Run($action) {
        $App=  App::getInstance();
        //задаем  действиt по умолчанию
        $actionname = 'actionDefault';
        $this->action = 'default';
        //если действие задано параметром
        if (!empty($action)) { 
           $actionname = 'action'.$action; //формируем имя класса
           $this->action = $action; 
        }
        //проверка сущестования метода действия
        if (method_exists($this,$actionname) ){        
            $this->$actionname(); //если существует - выполняем
        }
        else {
            //если нет - отдаем ошибку
            header("HTTP/1.1 404 Not Found");
            die($App->t("URL does not found")); 
        }
    }  
    //действие по умолчанию - если не задано
    function actionDefault() {       
        //генерируем и выводим страницу
        $this->RenderPage('','');
    }    
    //формирование блока 
    //$params - переменные шаблона
    //$template - файл шаблона
    //$commontempalte - true если общий шаблон 
    function RenderTemplate($params='', $template, $commontempalte=false) {
        //новый кдас шаблонизатора
        $tpl = new Template;
        //экземпляр приложения
        $App=App::getInstance();
        //если общий шаблон, ищем в $App->path_to_views, иначе в подкаталоге шаблонов контроллера
        if ($commontempalte===false){
            $templatedir = $_SERVER['DOCUMENT_ROOT'].$App->path_to_views.$this->controllername().'/';
        }
        else {
            $templatedir = $_SERVER['DOCUMENT_ROOT'].$App->path_to_views;
        }
        //если шаблон не задан или не удалось загрузить - отдаем ошибку 404 
        if (!isset($template) || $tpl->getTpl($templatedir.$template)===false) {          
                echo $App->t('Template file ').$template.$App->t(" not found");
                die();
        }
        //заполняем переменные в шаблоне
        if (is_array($params)!==false){
            foreach ($params as $key => $value) {
                $tpl->setValue(strtoupper ($key),$value); 
            }          
        }
        //формирование блока
        $tpl->tplParse(); 
        //возврат сформированного блока
        return $tpl->html; 
    }
    //формирование блока главного меню 
    function RenderMainMenu() {
       // session_start();
        $App = App::getInstance();
        //подблоки меню, зависящие от состояния авторизации пользователя
        $logoutoption = '';
        $loginoption = '';
       
        //роутинг пунктов главного меню 
        $menuoptions = [
            'default/default'=>'',
            'user/register' => '',
             'user/default' => ''];
      
        //простановка признака активного пункта меню
        $menuoptions[$this->ControllerName().'/'.$this->action] = 'active';
        //если пользователь авторизован
        if ($_SESSION['authuid']!=='guest'){
            //создаем объект пользователя
            $User = new Users;
            //загрузка модели по заданным параметрам
            $User->getModel(['uid'=>$_SESSION['authuid']]);  
            //формирование блока logout
            $logoutoption = $this->RenderTemplate([
                    'HTTP_HOST' => 'http://'.$_SERVER['HTTP_HOST'],
                    'uid'=>$User->uid,
                    'profopt' => $menuoptions['user/default']],
                    'logout.tpl',true);
            //формирование блока login                                 
            $loginoption = $this->RenderTemplate(['username'=>$User->firstname,'uid'=>$User->uid],'login.tpl',true);
        }
                                      
        //генерим главное меню
        $params = [
            'mainopt' => $menuoptions['default/default'],
            'regopt' => $menuoptions['user/register'],         
            'logout' => $logoutoption,
            'login' => $loginoption,
            'HTTP_HOST' => 'http://'.$_SERVER['HTTP_HOST'],
            'lang' => $_SESSION['lang']
        ];
        //формирование блока
        $mainmenu = $this->RenderTemplate($params,'mainmenu.tpl',true);
        
        return $mainmenu;
     }
     
    //генерация страниц
     //$content - содержание
     //$title - заголовок
    function RenderPage($content='', $title = '') {
        //экземпляр приложения
        $App = App::getInstance();
      //  session_start();
        //подготовка массива для опций переключения языка       
        foreach ($App->languages as $key => $value) {
            $languageoptions[$value]='';
        }
      
        //полный путь к главному лейауту в $App->main_layout
        $pathtotemplate =  $_SERVER['DOCUMENT_ROOT'].$App->path_to_views.$App->main_layout;
        //экземпляр шаблонизатора  
        $tpl = new Template;
        //если не удалось загрузить шаблон страницы - отдаем 404 ошибку
        if ($tpl->getTpl($pathtotemplate)===false) {          
            echo $App->t("Main layout ").$App->main_layout.$App->t(" not found");
            die();
        }
        //подключение файла сообщений об ошибке валидации полей формы для русского языва
        if ($_SESSION['lang']==='RU')  $tpl->setValue('VALIDATERUMESSAGES', '<script src="{HTTP_HOST}/js/messages_ru.js" type="text/javascript"></script>');
        //генерация главного меню             
        $mainmenu = $this->RenderMainMenu();
        //установка признака выбора активного переключаетля языка
        $languageoptions[$_SESSION['lang']] = 'btn btn-xs btn-primary';  
        
        //заполнение переменных шаблона
        $tpl->setValue('RUOPT', $languageoptions['RU']);
        $tpl->setValue('ENOPT', $languageoptions['EN']);
        
        $tpl->setValue('MAINMENU',$mainmenu);
        $tpl->setValue('TITLE',$title);  
        $tpl->setValue('LANG',$_SESSION['lang']);  
        $tpl->setValue('CONTENT',$content);   
        $tpl->setValue('HTTP_HOST','http://'.$_SERVER['HTTP_HOST']);  
        //формирование страницы
        $tpl->tplParse(); 
        //вывод страницы
        echo $tpl->html; 
    }
    //функция проверки прав доступа  к заданному действию
    //$action - действие
    //$pageuserid - идентификатор пользователя
    function CheckAccess($action,$pageuserid='') {
        session_start();
        $App = App::getInstance();
        //проверяемое действие
        $accessrule = $this->accessRules()[$action];
            $rezult = true;
            switch ($accessrule) {
                case 'auth': //обязательная авторизация
                    if ($_SESSION['authuid'] === $App->default_user) $rezult = false; 
                    break;
                case '*':   //доступ всем                
                    break;
                case 'private': // доступ только конкретному авторизованному пользователю
                    if ($_SESSION['authuid'] !== $pageuserid) $rezult = false;   
                    break;
                default:
                    break;
            }
            
        return $rezult;
    }
    
       //главный шаблон
    protected $mainlayout;
    //текущее действие
    protected $action;  
  
}
