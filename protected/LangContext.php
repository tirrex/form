<?php
//класс  докализации текста и сообщений
class LangContext {
    //массив сообщений с ключами - фразами на английском
    public static $messages = [
          'RU' => [
              'URL does not found' => 'URL не найден на сервере',
              'Template file ' => 'Файл шаблона ',
              ' not found' => ' не найден',
              'Main layout ' => 'Главный лейаут страницы ',
              'Login form' => 'Форма входа',
              'Form validation error, please, check input data' => 'Ошибка валидации формы, пожалуйста, проверьте введенные данные!',
              'Providing login and password do not match any registered user, please, check input data or Register!' => 'Введенные имя и пароль не соответствуют ни одному зарегистрированному пользователю, пожалуйста, проверьте введенную информацию или Зарегистрируйтесь!',
              'Error!' => 'Ошибка!' ,
              'User profile' => 'Профиль пользователя',
              'Access denied'=>'Доступ запрещен',
              'Requested page does not found' => 'Запрашиваемая страница не найдена',
              'User registration form' => 'Регистрация нового пользователя',
              'User registration error' => 'Ошибка при регистрации пользователя',
              'Registration successful'=>'Регистрация успешно завершена',
              ' Value: ' => ' Значение: ',
              ' is already present in database, please provide another one ' => ' уже имеется в базе данных, пожалуйста, введите другое. ',
              ' Field ' => ' Поле ',
              ' is required ' => ' является обязательным. ',
              ' must be a string ' => ' должно быть строкой. ',
              'Email field value does not match the format' => 'Значение поля email не соответствует формату.',
              'MySQL connection error: '=>'Ошибка подключения к MySQL серверу: ',
              'Email'=>'Email',
              'Password retype' => 'Повторить пароль',
              'Password' => 'Пароль',
              'Login name' => 'Логин',
              'Login' => 'Вход',
              'First name' => 'Имя',
              'Last name' => 'Фамилия',             
              'Register' => 'Регистрация',
              'Congratulation!' => 'Поздравляем!',
              'Test task' => 'Тестовое задание',
              'Home' => 'Главная',
              'Register' => 'Регистрация пользователя',
              'Logout' => 'Выход',
              'You are logged out!' => 'Вы вышли из системы!',
              'Nobody have been logged in!' => 'Никто не был залогинен!',
              'First name' => 'Имя', 
              'Last Name' => 'Фамилия',
              'Profile' => 'Аккаунт',
              'Hello' => 'Здравствуйте',
              'Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.' => 'Неверный формат файла! Допустимые форматы: JPG, JPEG, PNG или GIF.',
              'Maximum File Size Limit is 1MB.' => 'Максимальный размер файла: 1 Мб',
              'Browse' => 'Обзор',
              'Pick a picture' =>  'Выберите картинку',
              'Uploaded picture' => 'Загруженное изображение' ,
              ' is not allowed extension, please choose file with one of theese formats: jpg, png, gif' => ' не является допустимым форматом файла, пожалуйста, выберите формат файла из списка: jpg, png. gif'
              
          ]  
            
        ];
    //получение перевода строки на англ языке по ключу - фразе в зависимости от установленного в сессии языка
    //$emessage - текст на английском
    public static function getMessage($emessage) {
        $App=App::getInstance();
     //   session_start();
        //если никакой язык не установлен - выбираем язык по умолчанию
        if (isset($_SESSION['lang'])) {
            $currlanguage = $_SESSION['lang'];
        }
        else {
            $currlanguage = $App->default_language;
        }
               
       //если установен английский - возвращаем сообщение без изменений, иначе - ищем перевод
        if ($currlanguage === 'EN') return $emessage;
        return LangContext::$messages[$currlanguage][$emessage];
    }
    
}
