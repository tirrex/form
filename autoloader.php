<?php
//функция автозагрузки классов
function service_autoloader($class) {
        
    //каталоги с автозагружаемыми классами
    $directoryes = array(
        'protected/',
        'protected/controllers/',
        'protected/models/'
    );

    //перебор каталогов
    foreach($directoryes as $directory)
    {
        //если класс существует
        if(file_exists($directory.$class . '.php'))
        {
            //загружаем класс
            require_once($directory.$class . '.php');
            return;
        }   
    }
    
}
//регистрация автозагрузчика
spl_autoload_register('service_autoloader');